// function for input password when condition click button delete in index page
function onClickDel(item_id) {

    // initialize value input passsword
    const passInput = $('#inputPassword' + item_id).val();
    // set null when condition redirect to index
    $('#inputPassword' + item_id).val("");
    // get detail data bulletin by ajax
    $.ajax({
        url: 'http://timedoor-challenge.test/api/detail/bulletin/' + item_id,
        type: 'get',
        dataType: 'json',
        success: function(response) {

            // get data detail from response api
            const id_item_del = response.data.id;
            const pass_item_del = response.data.password;
            const body_item_del = response.data.body;
            const title_item_del = response.data.title;

            // condition value password input macthing wtih value password from database
            if (pass_item_del == null) {
                $('#form-group-del-pass').hide();

                $('#btnEditDelModal').hide();
                $('#btnDelModal').hide();
                $('#btnCancelModalDel').hide();
                $('#btnBackModalDel').show();

                $('#msgDel').hide();
                $('#erroMsgPassDel').hide();
                $('#msgReadOnlyDel').show();

                $('#titleModalDel').val(title_item_del)
                $('#bodyModalDel').val(body_item_del)

            } else if (passInput == pass_item_del) {
                $('#form-group-del-pass').hide();

                $('#btnEditDelModal').show();
                $('#btnDelModal').hide();
                $('#btnCancelModalDel').show();
                $('#btnBackModalDel').hide();

                $('#msgDel').show();
                $('#erroMsgPassDel').hide();
                $('#msgReadOnlyDel').hide();

                $('#titleModalDel').val(title_item_del)
                $('#bodyModalDel').val(body_item_del)

                $('#idbulletinModalDel').val(id_item_del);
            } else {
                $('#form-group-del-pass').show();

                $('#btnEditDelModal').hide();
                $('#btnDelModal').show();
                $('#btnCancelModalDel').show();
                $('#btnBackModalDel').hide();

                $('#msgDel').hide();
                $('#erroMsgPassDel').show();
                $('#msgReadOnlyDel').hide();

                $('#titleModalDel').val(title_item_del)
                $('#bodyModalDel').val(body_item_del)

                $('#idbulletinModalDel').val(id_item_del);
            }
        }
    })

    // add timer for diplaay modal
    setTimeout(function() {
        $('#deleteModal').modal('show');
    }, 1000);
}


// function for input password again  when condition click button delete in modal
function onClickCheckPassDel() {

    const idBulletin = $('#idbulletinModalDel').val();
    const passInputAgain = $('#inputPasswordModalDel').val();

    $('#inputPasswordModalDel').val("");
    $.ajax({
        url: 'http://timedoor-challenge.test/api/detail/bulletin/' + idBulletin,
        type: 'get',
        dataType: 'json',
        success: function(response) {
            const passTrue = response.data.password;
            if (passInputAgain == passTrue) {
                $('#btnDelModal').hide();
                $('#btnEditDelModal').show();
                $('#erroMsgPassDel').hide();
                $('#form-group-del-pass').hide();
                $('#msgDel').show();
            } else {
                $('#btnDelModal').show();
                $('#btnEditDelModal').hide();
                $('#erroMsgPassDel').show();
                $('#form-group-del-pass').show();
                $('#msgDel').hide();
            }
        }
    });
}

// function for input password when condition click button edit in index page
function onClickEdit(item_id) {

    const passInputEdit = $('#inputPassword' + item_id).val()
    $('#inputPassword' + item_id).val("");

    $.ajax({
        url: 'http://timedoor-challenge.test/api/detail/bulletin/' + item_id,
        type: 'get',
        dataType: 'json',
        success: function(response) {
            const id_item = response.data.id;
            const title_item = response.data.title;
            const body_item = response.data.body;
            const pass_item = response.data.password;
            const new_image_item = response.data.new_image;

            if (pass_item == null) {
                $('#btnBackModal').show();
                $('#form-group-pass').hide();
                $('#btnSaveModal').hide();
                $('#btnEditModal').hide();
                $('#btnCloseModal').hide();

                $('#idbulletinModalEdit').val(id_item);
                $('#titleModal').val(title_item);
                $('#bodyModal').val(body_item);
                $('#imageModal').attr('src', 'storage/user/images/' + new_image_item);

                $('#titleModal').attr('disabled', true);
                $('#bodyModal').attr('disabled', true);
                $('#buttonModalImage').prop('disabled', true);
                $('#checkModal').attr('disabled', true);

                $('#msgReadOnlyEdit').show();

            } else if (passInputEdit == pass_item) {
                $('#form-group-pass').hide();
                $('#btnSaveModal').show();
                $('#btnEditModal').hide();
                $('#btnBackModal').hide();
                $('#btnCloseModal').show();

                $('#idbulletinModalEdit').val(id_item);
                $('#titleModal').val(title_item);
                $('#bodyModal').val(body_item);
                $('#imageModal').attr('src', 'storage/user/images/' + new_image_item);

                $('#titleModal').attr('disabled', false);
                $('#bodyModal').attr('disabled', false);
                $('#buttonModalImage').prop('disabled', false);
                $('#checkModal').attr('disabled', false);

                $('#msgReadOnlyEdit').hide();

            } else {
                $('#form-group-pass').show();
                $('#btnSaveModal').hide();
                $('#btnEditModal').show();
                $('#btnBackModal').hide();
                $('#btnCloseModal').show();

                $('#erroMsgPassEdit').show();
                $('#msgReadOnlyEdit').hide();

                $('#idbulletinModalEdit').val(id_item);
                $('#titleModal').val(title_item);
                $('#bodyModal').val(body_item);
                $('#imageModal').attr('src', 'storage/user/images/' + new_image_item);

                $('#titleModal').attr('disabled', true);
                $('#bodyModal').attr('disabled', true);
                $('#buttonModalImage').prop('disabled', true);
                $('#checkModal').attr('disabled', true);

            }
        }
    });

    setTimeout(function() {
        $('#editModal').modal('show');
    }, 1000);
}

// function for input password again  when condition click button edit in modal
function onClickCheckPassEdit() {

    const idBulletinEdit = $('#idbulletinModalEdit').val();
    const passInputAgainEdit = $('#inputPasswordModalEdit').val();

    $('#inputPasswordModalEdit').val("");

    $.ajax({
        url: 'http://timedoor-challenge.test/api/detail/bulletin/' + idBulletinEdit,
        type: 'get',
        dataType: 'json',
        success: function(response) {
            const pass_data = response.data.password;
            if (passInputAgainEdit == pass_data) {
                $('#form-group-pass').hide();
                $('#btnSaveModal').show();
                $('#btnEditModal').hide();
                $('#btnBackModal').hide();
                $('#btnCloseModal').show();

                $('#erroMsgPassEdit').hide();
                $('#msgReadOnlyEdit').hide();

                $('#titleModal').attr('disabled', false);
                $('#bodyModal').attr('disabled', false);
                $('#buttonModalImage').prop('disabled', false);
                $('#checkModal').attr('disabled', false);

            } else {
                $('#form-group-pass').show();
                $('#btnSaveModal').hide();
                $('#btnEditModal').show();
                $('#btnBackModal').hide();
                $('#btnCloseModal').show();

                $('#erroMsgPassEdit').show();
                $('#msgReadOnlyEdit').hide();

            }
        }
    });
}

//function for condition when click button close will be reload page index
function onClickClose() {
    window.location.reload();
}

//#################################################################################//
// INPUT TYPE FILE
$(document).on('change', '.btn-file :file', function() {
    var input = $(this),
        numFiles = input.get(0).files ? input.get(0).files.length : 1,
        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
    input.trigger('fileselect', [numFiles, label]);
});

$(document).ready(function() {
    $('.btn-file :file').on('fileselect', function(event, numFiles, label) {
        var input = $(this).parents('.input-group').find(':text'),
            log = numFiles > 1 ? numFiles + ' files selected' : label;

        if (input.length) {
            input.val(log);
        } else {
            if (log) alert(log);
        }
    });
});