
<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "admin" middleware group. Now create something great!
|
*/

Route::get('/login', 'AuthAdmin\LoginController@showLoginForm')->name('login');
Route::post('/login', 'AuthAdmin\LoginController@login')->name('login.submit');

Route::group(['middleware' => 'admin'], function () {
    Route::get('/', function () {
        return redirect('/admin/dashboard');
    });
    Route::post('/logout','AuthAdmin\LoginController@logout')->name('logout');
    Route::get('/{bulletin}/delete/item','Admin\DashboardController@destroyItem')->name('dashboard.delete.item');
    Route::get('/{bulletin}/delete/image','Admin\DashboardController@destroyImage')->name('dashboard.delete.image');
    Route::post('/delete/all','Admin\DashboardController@destroyAll')->name('dashboard.delete.all');
    Route::get('/{id}/restore/item','Admin\DashboardController@restoreItem')->name('dashboard.restore.item');
    Route::get('/dashboard','Admin\DashboardController@index')->name('dashboard');
});
