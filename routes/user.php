
<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| User Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "user" middleware group. Now create something great!
|
*/

Route::post('email/resend','Auth\VerificationController@resend')->name('verification.resend');
Route::get('email/verify','Auth\VerificationController@show')->name('verification.notice');
Route::get('email/verify/{id}/{hash}','Auth\VerificationController@verify')->name('verification.verify');
Route::post('login','Auth\LoginController@login');
Route::get('login','Auth\LoginController@showLoginForm')->name('login');
Route::post('logout','Auth\LoginController@logout')->name('logout');
Route::get('password/confirm','Auth\ConfirmPasswordController@showConfirmForm')->name('password.confirm');
Route::post('password/confirm','Auth\ConfirmPasswordController@confirm');
Route::post('register','Auth\RegisterController@register');
Route::get('register','Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register/confirm','Auth\RegisterController@confirmRegister')->name('register.confirm');

// bulletin
Route::get('/','user\BulletinController@index')->name('index')->middleware('user');
Route::post('bulletin/store','user\BulletinController@store')->name('bulletin.store');
Route::post('bulletin/check/{bulletin}/edit','user\BulletinController@checkPasswordEdit')->name('bulletin.check.edit');
Route::post('bulletin/update/{bulletin}/', 'user\BulletinController@update')->name('bulletin.update');
Route::post('bulletin/check/{bulletin}/delete','user\BulletinController@checkPasswordDelete')->name('bulletin.check.delete');
Route::post('bulletin/delete/{bulletin}','user\BulletinController@destroy')->name('bulletin.delete');
