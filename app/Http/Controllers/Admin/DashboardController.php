<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Bulletin;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    /**
     * Display data bulletins in dashboard admin.
     *
     * @param Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
    */
    public function index(Request $request)
    {
        $bulletins = Bulletin::whereTitleLike($request)->whereBodyLike($request)->withImage($request)->withStatus($request)->latest()->paginate(20);

        return view('admin.pages.index',compact('bulletins'));
    }

    /**
     * Delete the spesific item bulletin.
     *
     * @param Illuminate\Http\Request $request
     * @param App\Models\Bulletin $bulletin
     * @return \Illuminate\Http\Response
    */
    public function destroyItem(Bulletin $bulletin)
    {
        $bulletin->delete();

        return redirect()->back()->with('success','Item bulletin berhasil dihapus');
    }

    /**
     * Delete only image item bulletin in storage.
     *
     * @param Illuminate\Http\Request $request
     * @param App\Models\Bulletin $bulletin
     * @return \Illuminate\Http\Response
    */
    public function destroyImage(Bulletin $bulletin)
    {
        $bulletin->update(['image'=> null]);

        return redirect()->back()->with('success','Gambar bulletin berhasil dihapus');
    }

    /**
     * Delete the listing bulletins with checkbox
     *
     * @param Illuminate\Http\Request $request
     * @param App\Models\Bulletin $bulletin
     * @return \Illuminate\Http\Response
    */
    public function destroyAll(Request $request)
    {
        if (is_null($request->idItem)) {
            return redirect()->back();
        }

        Bulletin::destroy($request->idItem);

        return redirect()->back()->with('success','Item bulletin yang dipilih berhasil dihapus');
    }

    /**
     * Restoring the spesific bulletins
     *
     * @param Illuminate\Http\Request $request
     * @param App\Models\Bulletin $bulletin
     * @param mixed $id
     * @return \Illuminate\Http\Response
    */
    public function restoreItem($id)
    {
        Bulletin::onlyTrashed()->where('id', $id)->restore();

        return redirect()->back()->with('success','Item bulletin yang dipilih berhasil di pulihkan');
    }

}
