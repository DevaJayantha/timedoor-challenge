<?php

namespace App\Http\Controllers\User;

use App\Models\Bulletin;
use App\Http\Controllers\Controller;
use App\Http\Requests\BulletinDelFormRequest;
use App\Http\Requests\BulletinEditFormRequest;
use App\Http\Requests\BulletinFormRequest;
use Illuminate\Http\Request;

class BulletinController extends Controller
{

    public function index()
    {
        $bulletins = Bulletin::latest()->paginate(5);
        return view('user.pages.index',compact('bulletins'));
    }

    public function store(BulletinFormRequest $request)
    {
        $bulletins = new Bulletin($request->all());

        if (auth()->check()) {
            $bulletins->user()->associate(auth()->user());
        }

        $bulletins->save();

        return redirect()->back()->with('success','berhasil disimpan datanya');
    }

    public function checkPasswordDelete(Request $request, Bulletin $bulletin)
    {
        $isPasswordVerified  = $bulletin->password ? $bulletin->verifyPassword($request->passDetail) : false;

        return redirect()->back()->with('delete',[
            'bulletin'             => $bulletin,
            'plainPassword'        => $request->passDetail,
            'isPasswordVerified'   => $isPasswordVerified
        ]);
    }

    public function checkPasswordEdit(Request $request, Bulletin $bulletin)
    {
        session(['bulletin' => $bulletin]);
        $isPasswordVerified = $bulletin->password ? $bulletin->verifyPassword($request->passDetail) : false;

        return redirect()->back()->withInput()->with('edit',[
            'plainPassword'        => $request->passDetail,
            'isPasswordVerified'   => $isPasswordVerified
        ]);
    }

    public function update(BulletinEditFormRequest $request, Bulletin $bulletin)
    {
        session(['bulletin' => $bulletin]);

        if (!auth()->check()) {
            if (is_null($bulletin->password) || !$bulletin->verifyPassword($request->passDetail) ){
                return redirect()->back()->with('edit',['isPasswordVerified' => false]);
            }
        }

        $bulletin->fill($request->only('name','title','body'));

        if  ($request->hasFile('image')) {
            $bulletin->image = $request->image;
        }

        if ($request->checkbox) {
            $bulletin->image = null;
        }

        $bulletin->save();

        return redirect()->back()->with('success','berhasil diupdate datanya');
    }

    public function destroy(BulletinDelFormRequest $request, Bulletin $bulletin)
    {
        if (!auth()->check()) {
            if (is_null($bulletin->password) || !$bulletin->verifyPassword($request->passDetail) ){
                return redirect()->back()->with('delete',['bulletin'=> $bulletin,'isPasswordVerified' => false]);
            }
        }

        $bulletin->delete();

        return redirect()->back()->with('success','berhasil dihapus datanya');
    }

}


