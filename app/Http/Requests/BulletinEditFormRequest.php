<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BulletinEditFormRequest extends FormRequest
{

    public $validator = null;
    public $errorBag = 'update';
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $bulletin = $this->route('bulletin');

        if (auth()->check()) {
            return $bulletin->user_id == auth()->id();

        }

        return empty($bulletin->user_id);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'name'      => 'nullable|min:3|max:16',
            'title'     => 'required|min:10|max:32',
            'body'      => 'required|min:10|max:200',
            'image'     => 'image|mimes:jpeg,jpg,png,gif|max:1000',
        ];
    }

    public function messages()
    {
        return [
            'name.min'            => 'Your name must be 3 to 16 characters long',
            'name.max'            => 'Your name must be 3 to 16 characters long',
            'title.required'      => 'Value title must be filled in',
            'body.required'       => 'Value body must be filled in',
            'title.min'           => 'Your title must be 10 to 32 characters long',
            'title.max'           => 'Your title must be 10 to 32 characters long',
            'body.min'            => 'Your body must be 10 to 200 characters long',
            'body.max'            => 'Your body must be 10 to 200 characters long',
            'image.image'         => 'Your input only image format',
            'image.max'           => 'Your image is only valid 1MB or less',
            'image.mimes'         => 'Your image is only valid .jpeg, .jpg, .png, .gif',
        ];
    }

}
