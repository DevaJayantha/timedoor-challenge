<?php

namespace App\Models;

use App\Traits\ImageManipulationTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Hash;

class Bulletin extends Model
{
    use ImageManipulationTrait;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
    */
    protected $fillable = [
        'user_id',
        'name',
        'title',
        'body',
        'image',
        'password'
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
    */
    protected $appends  = [
        'url_image',
        'name_with_id'
    ];

    /**
     * Bootstrap the model and its traits.
     *
     * @return void
     */
    protected static function booted()
    {
        static::saving(function ($model) {
            $model->saveImage();
        });

        static::deleting(function($model) {
            $model->onDelete();
        });
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function getNameWithIdAttribute() {
        if (is_null($this->name)) {
            return "No Name";
        }

        if (is_null($this->user_id)) {
            return "{$this->name}";
        }

        return "{$this->name} [ID:{$this->user_id}] ";
    }

    public function setPasswordAttribute($value)
    {
        if ($value) {
            $this->attributes['password'] = bcrypt($value);
        }
    }

    public function getUrlImageAttribute()
    {
        if (is_null($this->image)) {
            return asset('asset/user/images/no-image.png');
        }

        return asset('storage/user/images/'.$this->image);
    }

    public function verifyPassword($newPass):bool
    {
        return Hash::check($newPass, $this->password);
    }

    public function pathImage()
    {
        return 'public/user/images/';
    }

    public function scopeWhereTitleLike($query, $search)
    {
        return $query->where('title','LIKE', '%' . $search->title . '%');
    }

    public function scopeWhereBodyLike($query, $search)
    {
        return $query->Where('body','LIKE','%'.$search->body.'%');
    }

    public function scopeWithImage($query, $search)
    {
        if ($search->imageOption === "1") {
            return $query->whereNotNull('image');
        }

        if ($search->imageOption === "2") {
            return $query->whereNull('image');
        }

        return $query;
    }


    public function scopeWithStatus($query, $search)
    {
        if ($search->statusOption === "1") {
            return $query;
        }

        if ($search->statusOption === "2") {
            return $query->onlyTrashed();
        }

        return $query->withTrashed();
    }



}
