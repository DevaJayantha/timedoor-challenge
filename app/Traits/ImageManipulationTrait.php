<?php

namespace App\Traits;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\ModelNotFoundException;

trait ImageManipulationTrait {


    public function uploadImage()
    {
        $extension      = $this->image->getClientOriginalName();
        $fillname       = 'bulletin_' . date('YmdHis') . '_' .$extension;
        Storage::putFileAs($this->pathImage(), $this->image, $fillname);

        if ($this->isDirty('image')) {
            $this->deleteImage($this->pathImage().$this->getOriginal('image'));
        }

        $this->image  = $fillname;
    }

    public function saveImage()
    {
        if (!$this->isDirty('image')) {
            return false;
        }

        return $this->saveNewImage();
    }

    public function saveNewImage()
    {
        if (is_null($this->image)) {
            $this->deleteImage($this->pathImage().$this->getOriginal('image'));
        } elseif ($this->isDirty('image') ) {
            $this->uploadImage();
        } else {
            throw new ModelNotFoundException('condition not found');
        }
    }

    public function onDelete()
    {
        $pathImage = $this->pathImage().$this->image;
        $this->image = NULL;
        $this->deleteImage($pathImage);
        $this->save();
    }

    public function deleteImage($model)
    {
        Storage::delete($model);
    }

    public function setNull($image)
    {
        $this->$image = NULL;
        $this->save();
    }

}

?>
