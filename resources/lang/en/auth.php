<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'email' => 'Email address not found',
    'password' => 'Incorrect password',
    'verification' => 'Email not verification',
    'failed' => 'Login Failed, Please Try Again',

];
