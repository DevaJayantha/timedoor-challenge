@extends('user.master')

@section('body')
<div class="box login-box">
    <div class="login-box-head">
        <h1 class="mb-5">403</h1>
        <p class="text-lgray">Oops...This action is unauthorized.</p>
    </div>
    <div class="login-box-footer">
        <div class="text-right">
            <a href="{{ route('index') }}" class="btn btn-primary">Home</a>
        </div>
    </div>
</div>

@endsection
