@extends('user.master')

@section('body')

<div class="box login-box">
    <div class="login-box-head">
        <h1 class="mb-5">Register</h1>
    </div>
    <form method="POST" action="{{ route('register') }}">
        @csrf
        <div class="login-box-body">
            <div class="form-group">
                <input type="text" class="form-control" name="name" value="{{ $request['name'] }}"  readonly>
            </div>
            <div class="form-group">
                <input type="email" class="form-control" name="email" value="{{ $request['email'] }}" readonly>
            </div>
            <div class="form-group">
                <input type="text" class="form-control" value="{{ $request['password'] }}" name="password" readonly>
            </div>
        </div>
        <div class="login-box-footer">
            <div class="text-right">
                <a href="{{ route('index') }}" class="btn btn-default">Back</a>
                <button type="submit" class="btn btn-primary">
                    {{ __('submit') }}
                </button>
            </div>
        </div>
    </form>
</div>

@endsection
