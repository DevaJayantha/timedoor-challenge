@extends('user.master')

@section('body')
<div class="box login-box">
    <div class="login-box-head">
        <h1 class="mb-5">Register</h1>
        <p class="text-lgray">Please fill the information below...</p>
    </div>
    <form method="POST" action="{{ route('register.confirm') }}">
        @csrf
        @if ($errors->isNotEmpty())
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <p>{{ $error }}</p>
                @endforeach
            </div>
        @endif
        <div class="login-box-body">
            <div class="form-group">
                <input type="text" class="form-control" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus placeholder="Name">
            </div>
            <div class="form-group">
                <input type="email" class="form-control" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="E-mail">
            </div>
            <div class="form-group">
                <input type="password" class="form-control" name="password" required autocomplete="new-password" placeholder="Password">
            </div>
            <div class="form-group">
                <input type="password" class="form-control" name="password_confirmation" required autocomplete="new-password" placeholder="Confirm Password">
            </div>
        </div>
        <div class="login-box-footer">
            <div class="text-right">
                <a href="{{ route('index') }}" class="btn btn-default">Back</a>
                <button type="submit" class="btn btn-primary">
                    {{ __('Register') }}
                </button>
            </div>
        </div>
    </form>
</div>

@endsection
