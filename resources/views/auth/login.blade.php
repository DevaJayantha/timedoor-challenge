@extends('user.master')

@section('body')
<div class="box login-box">
    <div class="login-box-head">
        <h1 class="mb-5">Login</h1>
        <p class="text-lgray">Please login to continue...</p>
    </div>
    <form method="POST" action="{{ route('login') }}">
        @csrf
        @if ($errors->isNotEmpty())
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <p>{{ $error }}</p>
                @endforeach
            </div>
        @endif
        <div class="login-box-body">
            <div class="form-group">
                <input type="email" class="form-control" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="Email">
            </div>
            <div class="form-group">
                <input type="password" class="form-control" name="password" required autocomplete="current-password" placeholder="Password">
            </div>
        </div>
        <div class="login-box-footer">
            <div class="text-right">
                <button type="submit" class="btn btn-primary">
                    {{ __('Login') }}
                </button>
            </div>
        </div>
    </form>
</div>
@endsection
