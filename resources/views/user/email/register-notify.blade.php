@component('mail::message')
@slot('header')
@component('mail::header', ['url' => config('app.url')])
{{ config('app.name') }}
@endcomponent
@endslot

Hi, Mr <strong>{{$name}}</strong> <br>
<p> To enable your account, please click in the following linl or
    copy it onto the address bar of your favaurite browser. </p> <br>

@component('mail::button', ['url' => $verificationUrl])
Verify Now
@endcomponent<br>
Please click within 24 hours.<br>

Warm Regards<br><br>
Admin
{{-- Footer --}}
@slot('footer')
@component('mail::footer')
© {{ date('Y') }} {{ config('app.name') }}. @lang('All rights reserved.')
@endcomponent
@endslot
@endcomponent
