<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Timedoor Challenge - Level 8</title>
    {{--  <meta name="_token" content="{{ csrf_token() }}">  --}}
    {{-- CSS --}}
    <link rel="stylesheet" type="text/css" href="{{ asset('asset/user/css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('asset/user/css/style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('asset/user/css/tmdrPreset.css')}}">
    {{-- CSS End --}}

    {{--  Add CSS Custom  --}}
    @yield('style-custom')
    {{--  End  --}}

    {{-- Javascript --}}
    <script type="text/javascript" src="{{ asset('asset/user/js/jquery.js')}}"></script>
    <script type="text/javascript" src="{{ asset('asset/user/js/bootstrap.min.js')}}"></script>
    {{-- Javascript End --}}

</head>
<body class="bg-lgray">
    @yield('navbar')
    <main>
        @yield('body')
    </main>
    @yield('js-custom')
    @yield('footer')
</body>
    @yield('modal')
    @yield('script')
</html>
