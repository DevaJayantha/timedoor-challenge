<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">Edit Item</h4>
            </div>
            <form action="{{ route('bulletin.check.edit', session()->get('bulletin')->id)}}" enctype="multipart/form-data" method="post">
                @csrf
                <div class="modal-body">
                    @if (is_null(session()->get('bulletin')->password))
                        <div class="alert alert-danger" role="alert">
                            This message can’t delete, because this message has not been set password.
                        </div>
                    @endif
                    <div class="form-group">
                        <label>Name</label>
                        <input id="titleModal" type="text" class="form-control" name="name" value="{{ session()->get('bulletin')->name }}" disabled>
                    </div>
                    <div class="form-group">
                        <label>Title</label>
                        <input id="titleModal" type="text" class="form-control" name="title" value="{{ session()->get('bulletin')->title }}" disabled>
                    </div>
                    <div class="form-group">
                        <label>Body</label>
                        <textarea id="bodyModal" name="body" rows="5" class="form-control" disabled>{{ session()->get('bulletin')->body }}</textarea>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-4">
                            <img class="img-responsive image-modal" alt="" id="imageModal" src="{{ session()->get('bulletin')->url_image }}">
                        </div>
                        <div class="col-md-8 pl-0">
                            <label>Choose image from your computer :</label>
                            <div class="input-group">
                                <input type="text" value="No file chosen" class="form-control upload-form" readonly>
                                <span class="input-group-btn">
                                    <span class="btn btn-default btn-file">
                                        <i class="fa fa-folder-open"></i>&nbsp;Browse <input type="file" id="buttonModalImage" name="image" accept=".png, .jpg, .jpeg, .gif" disabled>
                                    </span>
                                </span>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input id="checkModal" type="checkbox" name="checkbox" disabled>Delete image
                                </label>
                            </div>
                        </div>
                    </div>
                    @if (!is_null(session()->get('bulletin')->password))
                        <div class="form-group" id="form-group-pass">
                            <label id="textPasswordModal">Password</label>
                            <input type="password" class="form-control" name="passDetail">
                            <p class="small text-danger mt-5" id="erroMsgPassEdit" >*The passwords you entered do not match. Please try again.</p>
                        </div>
                    @endif
                </div>
                <div class="modal-footer">
                    @if (is_null(session()->get('bulletin')->password))
                        <center><button type="button" class="btn btn-primary btnClose" id="btnBackModalDel" data-dismiss="modal">Back Previous Page</button></center>
                    @endif
                    @if (!is_null(session()->get('bulletin')->password))
                        <button type="submit" name="action" value="edit" class="btn btn-info" id="btnEditModal">Edit</button>
                        <button type="button" class="btn btn-default btnClose" id="btnCloseModal" data-dismiss="modal">Close</button>
                    @endif
                </div>
            </form>
        </div>
    </div>
</div>
