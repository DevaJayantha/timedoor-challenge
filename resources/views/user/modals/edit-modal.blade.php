<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">Edit Item</h4>
            </div>
            <form action="{{ route('bulletin.update', session()->get('bulletin')->id ) }}" enctype="multipart/form-data" method="post">
                @csrf
                <div class="modal-body">
                    @if ($errors->update->isNotEmpty())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->update->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="form-group">
                        <label>Name (3 to 16 characters):</label>
                        <input id="titleModal" type="text" class="form-control" name="name" value="{{ old('name') ?? session()->get('bulletin')->name }}">
                    </div>
                    <div class="form-group">
                        <label>Title (10 to 32 characters):</label>
                        <input id="titleModal" type="text" class="form-control" name="title" value="{{ old('title') ?? session()->get('bulletin')->title }}">
                    </div>
                    <div class="form-group">
                        <label>Body (10 to 200 characters):</label>
                        <textarea id="bodyModal" name="body" rows="5" class="form-control">{{ old('body') ?? session()->get('bulletin')->body }}</textarea>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-4">
                            <img class="img-responsive image-modal" alt="" id="imageModal" src="{{ session()->get('bulletin')->url_image }}">
                        </div>
                        <div class="col-md-8 pl-0">
                            <label>Choose image from your computer :</label>
                            <div class="input-group">
                                <input type="text" value="No file chosen" class="form-control upload-form" readonly>
                                <span class="input-group-btn">
                                    <span class="btn btn-default btn-file">
                                    <i class="fa fa-folder-open"></i>&nbsp;Browse <input type="file" name="image" accept=".png, .jpg, .jpeg, .gif">
                                    </span>
                                </span>
                            </div>
                            <input type="hidden" name="passDetail" value="{{ session()->get('edit.plainPassword') ?? old('passDetail') }}">
                            <div class="checkbox">
                                <label>
                                    <input id="checkModal" type="checkbox" name="checkbox">Delete image
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" id="btnClose" data-dismiss="modal">Close</button>
                    <button type="submit" name="action" value="save" class="btn btn-primary" id="btnSaveModal">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>
