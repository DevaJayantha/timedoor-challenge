<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
          <h4 class="modal-title">Delete Data</h4>
        </div>
        <form action="{{ route('bulletin.check.delete', session()->get('delete.bulletin')->id)}}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="modal-body pad-20">
                @if (is_null(session()->get('delete.bulletin')->password))
                    <div class="alert alert-danger" role="alert">
                        This message can’t delete, because this message has not been set password.
                    </div>
                @endif
                <div class="form-group">
                    <label>Name</label>
                    <input id="titleModalDel" type="text" class="form-control" name="name" value="{{ session()->get('delete.bulletin')->name }}" readonly>
                </div>
                <div class="form-group">
                    <label>Title</label>
                    <input id="titleModalDel" type="text" class="form-control" name="title" value="{{ session()->get('delete.bulletin')->title }}" readonly>
                </div>
                <div class="form-group">
                    <label>Body</label>
                    <textarea id="bodyModalDel" name="body" rows="5" class="form-control" readonly>{{ session()->get('delete.bulletin')->body }}</textarea>
                </div>
                @if (!is_null(session()->get('delete.bulletin')->password))
                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" class="form-control" name="passDetail">
                        <p class="small text-danger mt-5" id="erroMsgPassDel" >*The passwords you entered do not match. Please try again.</p>
                    </div>
                @endif
            </div>
            <div class="modal-footer">
                @if (is_null(session()->get('delete.bulletin')->password))
                    <center><button type="button" class="btn btn-primary btnClose" data-dismiss="modal">Back Previous Page</button></center>
                @endif
                @if (!is_null(session()->get('delete.bulletin')->password))
                    <button type="button" class="btn btn-default btnClose" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-info" name="action" value="submit">Submit</button>
                @endif
            </div>
        </form>
      </div>
    </div>
</div>
