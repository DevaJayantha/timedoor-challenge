<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
          <h4 class="modal-title">Delete Data</h4>
        </div>
        <form action="{{ route('bulletin.delete', session()->get('delete.bulletin')->id ) }}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="modal-body pad-20">
                <div class="form-group">
                    <label>Name</label>
                    <input id="titleModalDel" type="text" class="form-control" name="name" value="{{ session()->get('delete.bulletin')->name }}" readonly>
                </div>
                <div class="form-group">
                    <label>Title</label>
                    <input id="titleModalDel" type="text" class="form-control" name="title" value="{{ session()->get('delete.bulletin')->title }}" readonly>
                </div>
                <div class="form-group">
                    <label>Body</label>
                    <textarea id="bodyModalDel" name="body" rows="5" class="form-control" readonly>{{ session()->get('delete.bulletin')->body }}</textarea>
                </div>
                <br>
                <input type="hidden" name="passDetail" value="{{ session()->get('delete.plainPassword') }}">
                <center><p>Are you sure want to delete this item?</p></center>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-danger" name="action" value="delete" id="btnEditDelModal">Delete</button>
            </div>
        </form>
      </div>
    </div>
</div>
