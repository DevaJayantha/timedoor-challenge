@extends('user.master')

@section('style-custom')
    <link rel="stylesheet" type="text/css" href="{{ asset('asset/user/css/font-awesome.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('asset/user/css/custom-index.css')}}">
@endsection

@section('body')
    <div class="section">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3 bg-white p-30 box">
                    <div>
                        <h1 class="text-green mb-30"><b>Level 8 Challenge</b></h1>
                        @if (session('success'))
                            <div class="alert alert-success alert-block">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>{{ session('success') }}</strong>
                            </div>
                        @endif
                        @if ($errors->store->isNotEmpty())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->store->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
                    <form action="{{ route('bulletin.store')}}" method="post" class="form" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label>Name</label>
                            <input type="text" name="name" class="form-control" value="{{ auth()->user()->name ?? old('name') }}">
                            <p class="small text-muted mt-5">*Title must be 3 to 16 characters long</p>
                        </div>
                        <div class="form-group">
                            <label>Title</label>
                            <input type="text" name="title" class="form-control" id="title" value="{{ old('title') }}" required>
                            <p class="small text-muted mt-5">*Title must be 10 to 32 characters long</p>
                        </div>
                        <div class="form-group">
                            <label>Body</label>
                            <textarea rows="5" name="body" class="form-control" id="body" required>{{ old('body') }}</textarea>
                            <p class="small text-muted mt-5">*Body must be 10 to 200 characters long</p>
                        </div>
                        <div class="form-group">
                            <label>Choose image from your computer :</label>
                            <div class="input-group">
                              <input type="text" class="form-control upload-form" value="No file chosen" readonly>
                              <span class="input-group-btn">
                                <span class="btn btn-default btn-file">
                                    <i class="fa fa-folder-open"></i>&nbsp;Browse <input type="file" name="image" accept=".png, .jpg, .jpeg, .gif">
                                </span>
                              </span>
                            </div>
                        </div>
                        @guest
                            <div class="form-group">
                                <label>Password</label>
                                <input type="password" class="form-control" name="password">
                            </div>
                        @endguest
                        <div class="text-center mt-30 mb-30">
                            <button class="btn btn-primary" type="submit">Submit</button>
                        </div>
                    </form>
                    <hr>
                    @foreach ($bulletins as $bulletin)
                        <div class="post">
                            <div class="clearfix">
                                <div class="pull-left">
                                    <h4 hidden data-id="{{ $bulletin->id }}">test</h4>
                                    <h2 class="mb-5 text-green"><b>{{ $bulletin->title }}</b></h2>
                                </div>
                                <div class="pull-right text-right">
                                    <p class="text-lgray">{{ $bulletin->created_at->format('d-m-Y') }}<br/><span class="small">{{ $bulletin->created_at->format('H:i') }}</span></p>
                                </div>
                            </div>
                            <h4 class="mb-20">{{ $bulletin->name_with_id }}<span class="text-id"></span></h4>
                            <p>{!! nl2br(e($bulletin->body)) !!}</p>
                            <div class="img-box my-10">
                                <img class="img-responsive img-post" src="{{$bulletin->url_image}}" alt="image" style="width: 50%!important">
                            </div>
                            <form class="form-inline mt-50" method="post" action="{{ route('bulletin.check.edit', $bulletin->id)}}">
                                @csrf
                                @guest
                                    @if (is_null($bulletin->user_id))
                                        <div class="form-group mx-sm-3 mb-2">
                                            <label class="sr-only">Password</label>
                                            <input type="password" class="form-control" name="passDetail" placeholder="Password" required>
                                        </div>
                                    @endif
                                @endguest
                                @if (auth()->id() === $bulletin->user_id)
                                    <button  type="submit" class="btn btn-default mb-2"><i class="fa fa-pencil p-3"></i></button>
                                    <button type="submit" name="action" value="delete" formaction="{{ route('bulletin.check.delete', $bulletin->id)}}" class="btn btn-danger mb-2"><i class="fa fa-trash p-3" ></i></button>
                                @endif
                            </form>
                        </div>
                    @endforeach
                    <div class="text-center mt-30">
                        {{ $bulletins->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('modal')
    @if (session()->has('edit'))
        @if (!session()->get('edit.isPasswordVerified') && !auth()->check())
            @include('user.modals.incorrect-edit-modal')
        @else
            @include('user.modals.edit-modal')
        @endif

        <script>
            $('#modal').modal('show');
        </script>
    @endif

    @if ($errors->update->isNotEmpty())
        @include('user.modals.edit-modal')
        <script>
            $('#modal').modal('show');
        </script>
    @endif

    @if (session()->has('delete'))
        @if (!session()->get('delete.isPasswordVerified') && !auth()->check())
            @include('user.modals.incorrect-delete-modal')
        @else
            @include('user.modals.delete-modal')
        @endif

        <script>
            $('#modal').modal('show');
        </script>
    @endif

@endsection


@section('script')
    <script>
        $('.btnClose').click(function(){
            location.reload();
        });
        // INPUT TYPE FILE
        $(document).on('change', '.btn-file :file', function() {
        var input = $(this),
            numFiles = input.get(0).files ? input.get(0).files.length : 1,
            label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
        input.trigger('fileselect', [numFiles, label]);
        });

        $(document).ready( function() {
        $('.btn-file :file').on('fileselect', function(event, numFiles, label) {
            var input = $(this).parents('.input-group').find(':text'),
                log = numFiles > 1 ? numFiles + ' files selected' : label;

            if( input.length ) {
            input.val(log);
            } else {
            if( log ) alert(log);
            }
        });
        });
    </script>
@endsection

@section('navbar')
<header>
    <nav class="navbar navbar-default" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <h2 class="font16 text-green mt-15"><b>Timedoor 30 Challenge Programmer</b></h2>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                @guest
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                    </li>
                    @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                        </li>
                    @endif
                @else
                    @if (Route::has('logout'))
                        <li class="nav-item">
                            <a class="dropdown-item" href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                                          document.getElementById('logout-form').submit();">
                             {{ __('Logout') }}
                         </a>
                         <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                            @csrf
                        </form>
                        </li>
                    @endif
                @endguest
                </ul>
            </div>
        </div>
    </nav>
</header>
@endsection

@section('footer')
<footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 0.1.0
    </div>
    <strong>Copyright &copy; {{ now()->year }} <a href="https://timedoor.net" class="text-green">Timedoor Indonesia</a>.</strong> All rights
    reserved.
</footer>
@endsection
