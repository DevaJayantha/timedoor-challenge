<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Timedoor Admin | Dashboard</title>

    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="{{ asset('asset/admin/plugin/bootstrap/bootstrap.css')}}">
    <link rel="stylesheet" href="{{ asset('asset/admin/plugin/font-awesome/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{ asset('asset/admin/plugin/Ionicons/ionicons.min.css')}}">
    <link rel="stylesheet" href="{{ asset('asset/admin/css/admin.css')}}">
    <link rel="stylesheet" href="{{ asset('asset/admin/css/tmdrPreset.css')}}">
    <link rel="stylesheet" href="{{ asset('asset/admin/css/custom.css')}}">
    <link rel="stylesheet" href="{{ asset('asset/admin/css/skin.css') }}">
    <link rel="stylesheet" href="{{ asset('asset/admin/plugin/bootstrap-datepicker/bootstrap-datetimepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('asset/admin/plugin/daterangepicker/daterangepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('asset/admin/plugin/datatable/datatables.min.css')}}">
    <link rel="stylesheet" href="{{ asset('asset/admin/plugin/selectpicker/bootstrap-select.css')}}">
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin sidebar-mini">
    @yield('body')
    @yield('modal')
    <script src="{{ asset('asset/admin/plugin/jquery/jquery.js')}}"></script>
    <script src="{{ asset('asset/admin/plugin/jquery/jquery-ui.min.js')}}"></script>
    <script>
      $.widget.bridge('uibutton', $.ui.button);
    </script>

    <script src="{{ asset('asset/admin/plugin/bootstrap/bootstrap.min.js')}}"></script>
    <script src="{{ asset('asset/admin/plugin/moment/moment.min.js')}}"></script>
    <script src="{{ asset('asset/admin/plugin/daterangepicker/daterangepicker.js')}}"></script>
    <script src="{{ asset('asset/admin/plugin/bootstrap-datepicker/bootstrap-datetimepicker.js')}}"></script>
    <script src="{{ asset('asset/admin/js/adminlte.min.js')}}"></script>
    <script src="{{ asset('asset/admin/plugin/datatable/datatables.min.js')}}"></script>
    <script src="{{ asset('asset/admin/plugin/ckeditor/ckeditor.js')}}"></script>
    <script src="{{ asset('asset/admin/plugin/selectpicker/bootstrap-select.js')}}"></script>

    <script>
      // BOOTSTRAP TOOLTIPS
      if ($(window).width() > 767) {
        $(function () {
          $('[rel="tooltip"]').tooltip()
        });
      };
    </script>
    @yield('script')
</body>
</html>
