@extends('admin.master')

@section('body')
    <div class="login-box">
        <div class="login-box-head">
            <h1>Login</h1>
            <p>Please login to continue...</p>
        </div>
        <form method="POST" action="{{ route('admin.login.submit') }}">
            @csrf
            @if ($errors->isNotEmpty())
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <p>{{ $error }}</p>
                    @endforeach
                </div>
            @endif
            <div class="login-box-body">
                <div class="form-group">
                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                    <p class="mt-5 small text-danger">*this field cant be empty</p>
                </div>
                <div class="form-group">
                    <input id="password" type="password" class="form-control" name="password" required autocomplete="current-password">
                    <p class="mt-5 small text-danger">*this field cant be empty</p>
                </div>
            </div>
            <div class="login-box-footer">
                <div class="text-right">
                    <button type="submit" class="btn btn-primary">
                        {{ __('Submit') }}
                    </button>
                </div>
            </div>
        </form>
    </div>
@endsection
