@extends('admin.master')

@section('body')
    <div class="wrapper">
        <header class="main-header">
            <a href="index2.html" class="logo">
                <span class="logo-mini"><b>T</b>D</span>
                <span class="logo-lg"><b>Timedoor</b> Admin</span>
            </a>
            @include('admin.template.nav')
        </header>
        <aside class="main-sidebar">
            <section class="sidebar">
                <ul class="sidebar-menu" data-widget="tree">
                    <li class="active">
                        <a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i>
                            <span>Dashboard</span>
                        </a>
                    </li>
                </ul>
            </section>
        </aside>
        {{--  <!-- Content Wrapper. Contains page content -->  --}}
        <div class="content-wrapper">
            <section class="content-header">
                <h1>Dashboard<small>Control panel</small></h1>
            </section>
            {{--  <!-- Main content -->  --}}
            <section class="content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box box-success">
                            <div class="box-header with-border">
                                <h1 class="font-18 m-0">Timedoor Challenge - Level 9</h1>
                            </div>
                            <div class="box-body">
                                <div class="bordered-box mb-20">
                                    <form class="form" role="form" method="get" action="{{ route('admin.dashboard') }}" enctype="multipart/form-data">
                                        @csrf
                                        <table class="table table-no-border mb-0">
                                            <tbody>
                                                <tr>
                                                    <td width="80"><b>Title</b></td>
                                                    <td>
                                                        <div class="form-group mb-0">
                                                            <input type="text" name="title" class="form-control" value="{{request()->get('title')}}">
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><b>Body</b></td>
                                                    <td>
                                                        <div class="form-group mb-0">
                                                            <input type="text" name="body" class="form-control" value="{{request()->get('body')}}">
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <table class="table table-search">
                                            <tbody>
                                                <tr>
                                                    <td width="80"><b>Image</b></td>
                                                    <td width="60">
                                                        <label class="radio-inline">
                                                            <input type="radio" name="imageOption" id="inlineRadio1" value="1" {{ request()->get('imageOption')=="1" ? "checked" : "" }}> with
                                                        </label>
                                                    </td>
                                                    <td width="80">
                                                        <label class="radio-inline">
                                                            <input type="radio" name="imageOption" id="inlineRadio2" value="2" {{ request()->get('imageOption')=="2" ? "checked" : "" }}> without
                                                        </label>
                                                    </td>
                                                    <td>
                                                        <label class="radio-inline">
                                                            <input type="radio" name="imageOption" id="inlineRadio3" value="3" {{ request()->get('imageOption')=="3" ? "checked" : "" }}> unspecified
                                                        </label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="80"><b>Status</b></td>
                                                    <td>
                                                        <label class="radio-inline">
                                                            <input type="radio" name="statusOption" id="inlineRadio1" value="1" {{ request()->get('statusOption')=="1" ? "checked" : "" }}> on
                                                        </label>
                                                    </td>
                                                    <td>
                                                        <label class="radio-inline">
                                                            <input type="radio" name="statusOption" id="inlineRadio2" value="2" {{ request()->get('statusOption')=="2" ? "checked" : "" }}> delete
                                                        </label>
                                                    </td>
                                                    <td>
                                                        <label class="radio-inline">
                                                            <input type="radio" name="statusOption" id="inlineRadio3" value="3" {{ request()->get('statusOption')=="3" ? "checked" : "" }}> unspecified
                                                        </label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><button type="submit" class="btn btn-default mt-10"><i class="fa fa-search"> Search </button></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </form>
                                </div>
                                @if (session('success'))
                                    <div class="alert alert-success alert-block">
                                        <button type="button" class="close" data-dismiss="alert">×</button>
                                        <strong>{{ session('success') }}</strong>
                                    </div>
                                @endif
                                @if (!$bulletins->isEmpty())
                                    <form action="{{ route('admin.dashboard.delete.all') }}" enctype="multipart/form-data" method="post" id="formTable">
                                        @csrf
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th><input type="checkbox" onclick="toggle(this);"></th>
                                                    <th>ID</th>
                                                    <th>Title</th>
                                                    <th>Body</th>
                                                    <th width="200">Image</th>
                                                    <th>Date</th>
                                                    <th width="50">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($bulletins as $bulletin)
                                                    <tr style="background-color:{{ $bulletin->trashed() ? 'rgb(232,232,232);':'transparent'}}">
                                                        <td>
                                                            @if (!$bulletin->trashed())
                                                                <input type="checkbox" class="checkboxItem" name="idItem[]" value="{{ $bulletin->id }}">
                                                            @endif
                                                        </td>
                                                        <td>{{ $bulletin->id }}</td>
                                                        <td>{{ $bulletin->title }}</td>
                                                        <td>{{ $bulletin->body }}</td>
                                                        <td>
                                                            @if(!empty($bulletin->image) && file_exists(public_path('storage/user/images/'.$bulletin->image)))
                                                                <img class="img-prev" src="{{$bulletin->url_image}}" >
                                                                <a id="delImg{{ $bulletin->id }}" data-id-del-img="{{ $bulletin->id }}" data-toggle="modal" data-target="#deleteImgModal" class="btn btn-danger ml-10 btn-img" rel="tooltip" title="Delete Image" onclick="deleteOfImageItem(this.id)"><i class="fa fa-trash"></i></a>
                                                            @endif
                                                        </td>
                                                        <td>{{ $bulletin->created_at->format('y/m/d') }}<br><span class="small">{{ $bulletin->created_at->format('H:m:s') }}</span></td>
                                                        <td>
                                                            @if ($bulletin->trashed())
                                                                <a id="restItem{{ $bulletin->id }}" data-id-rest-item="{{ $bulletin->id }}" data-toggle="modal" data-target="#restoreItem" class="btn btn-warning" onclick="restoreOfItem(this.id)"><i class="fa fa-share-square"></i></a>
                                                            @else
                                                                <a id="delItem{{ $bulletin->id }}" data-id-del-item="{{ $bulletin->id }}" data-toggle="modal" data-target="#deleteItemModal" onclick="deleteOfItem(this.id)" class="btn btn-danger" rel="tooltip" title="Delete"><i class="fa fa-trash"></i></a>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                        <a data-toggle="modal" id="" data-target="#deleteAll" class="btn btn-primary mt-5" rel="tooltip" >Delete Checked Items</a>
                                    </form>
                                @else
                                    <p class="text-center" style="background:#6DDA5F; color:#ffff; font-size:20px;">Bulletin Not Found</p>
                                @endif
                                <div class="text-center">
                                    {{ $bulletins->links() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        function deleteOfItem(id) {
            var id_bulletin_for_del_item =  document.querySelector('#'+id+'');
            document.getElementById('delete-item').action = id_bulletin_for_del_item.dataset.idDelItem+'/delete/item';
        }

        function deleteOfImageItem(id) {
            var id_bulletin_for_del_img =  document.querySelector('#'+id+'');
            document.getElementById('delete-img').action = id_bulletin_for_del_img.dataset.idDelImg+'/delete/image';
        }

        function restoreOfItem(id) {
            var id_bulletin_for_rest_item =  document.querySelector('#'+id+'');
            document.getElementById('restore-item').action = id_bulletin_for_rest_item.dataset.idRestItem+'/restore/item';
        }

        function deleteProcess() {
            document.getElementById('formTable').submit();
        }

        function toggle(source) {
            var checkbox = document.querySelectorAll('input[type="checkbox"]');
            for (var i=0; i < checkbox.length; i++) {
                if (checkbox[i] != source)
                    console.log(source.checked);
                    checkbox[i].checked = source.checked;
            }
        }

    </script>
@endsection

@section('modal')
    <div class="modal fade" id="deleteImgModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <div class="text-center">
                        <h4 class="modal-title" id="myModalLabel">Delete Image Bulletin</h4>
                    </div>
                </div>
                <div class="modal-body pad-20">
                    <p>Are you sure want to delete this image(s)?</p>
                </div>
                <form id="delete-img" enctype="multipart/form-data" method="get">
                    @csrf
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-danger">Delete</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="deleteItemModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <div class="text-center">
                        <h4 class="modal-title" id="myModalLabel">Delete Bulletin</h4>
                    </div>
                </div>
                <div class="modal-body pad-20">
                    <p>Are you sure want to delete this item(s)?</p>
                </div>
                <form id="delete-item" enctype="multipart/form-data" method="get">
                    @csrf
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-danger">Delete</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="restoreItem" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <div class="text-center">
                        <h4 class="modal-title" id="myModalLabel">Restore Bulletin</h4>
                    </div>
                </div>
                <div class="modal-body pad-20">
                    <p>Are you sure want to restore this item(s)?</p>
                </div>
                <form id="restore-item" enctype="multipart/form-data" method="get">
                    @csrf
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-danger">Restore</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="deleteAll" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <div class="text-center">
                        <h4 class="modal-title" id="myModalLabel">Delete Bulletin</h4>
                    </div>
                </div>
                <div class="modal-body pad-20">
                    <p>Are you sure want to Delete this item(s)?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-danger" onclick="deleteProcess()">Delete</button>
                </div>
            </div>
        </div>
    </div>
@endsection
