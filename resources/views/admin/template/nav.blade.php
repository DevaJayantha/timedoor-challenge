<nav class="navbar navbar-static-top">
    <!-- Sidebar toggle button-->
    <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
    </a>

    <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
            <!-- User Account: style can be found in dropdown.less -->
            <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <span class="hidden-xs">Hello, {{Auth::guard('admin')->user()->name}} </span>
                </a>
                <ul class="dropdown-menu">
                    <!-- User image -->
                    <li class="user-header">
                        <img src="{{ asset('asset/admin/img/user-ico.jpg')}}" class="img-circle" alt="User Image">
                        <p>Administrator</p>
                    </li>
                    <!-- Menu Footer-->
                    @if (Auth::guard('admin')->check())
                        <li class="nav-item">
                            <form action="{{ route('admin.logout')}}" method="post">
                                @csrf
                                <button type="submit" class="btn btn-inline-primary btn-block">Logout</button>
                            </form>
                        </li>
                    @endif
                </ul>
            </li>
        </ul>
    </div>
</nav>
